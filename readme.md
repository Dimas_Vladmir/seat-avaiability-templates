# Layout Template Seat Avaiability

Template Seat 
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing


```
    Installing Node by running this command npm install / yarn 

```
```
    Next Step you should install Gulp by running this command npm install --global gulp-cli
```

End with an example of getting some data out of the system or using it for a little demo

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Node](http://www.dropwizard.io/1.0.2/docs/) - Dependency Management
* [Gulp](https://maven.apache.org/) - Dependency Management
* [Sass / Scss](https://rometools.github.io/rome/) - Used to make styling

## Authors

* **Dimas Prasetyo** - *Initial work* - [FrontendDev](https://github.com/vladmir123)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.
