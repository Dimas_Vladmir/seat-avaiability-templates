"use-strict";

var gulp    = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');

gulp.task('html', function () {
    browserSync.reload();
});

gulp.task('serving', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('default', gulp.series('serving', function (){
    // gulp.watch('./app/scss/*.scss', ['sass', browserSync.reload]);
    gulp.watch('*.html', ['html', browserSync.reload]);
}));
	
gulp.task('vendorize',function() {
	gulp.src([
      './node_modules/milligram/dist/milligram.min.css',
    ])
    .pipe(gulp.dest('./vendor/miligram'))
});

gulp.task('normalize', function() {
    gulp.src([
      './node_modules/normalize.css/normalize.css',
    ])
    .pipe(gulp.dest('./vendor/normalize'))
});